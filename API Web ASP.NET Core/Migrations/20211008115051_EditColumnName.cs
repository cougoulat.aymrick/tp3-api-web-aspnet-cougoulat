﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Web_ASP.NET_Core.Migrations
{
    public partial class EditColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FILM_URLPHOTO",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_URLPHOTO");

            migrationBuilder.RenameColumn(
                name: "FILM_TITRE",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_TITRE");

            migrationBuilder.RenameColumn(
                name: "FILM_SYNOPSIS",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_SYNOPSIS");

            migrationBuilder.RenameColumn(
                name: "FILM_GENRE",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_GENRE");

            migrationBuilder.RenameColumn(
                name: "FILM_DUREE",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_DUREE");

            migrationBuilder.RenameColumn(
                name: "FILM_DATEPARUTION",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_DATEPARUTION");

            migrationBuilder.RenameColumn(
                name: "FILM_ID",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FLM_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FLM_URLPHOTO",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_URLPHOTO");

            migrationBuilder.RenameColumn(
                name: "FLM_TITRE",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_TITRE");

            migrationBuilder.RenameColumn(
                name: "FLM_SYNOPSIS",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_SYNOPSIS");

            migrationBuilder.RenameColumn(
                name: "FLM_GENRE",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_GENRE");

            migrationBuilder.RenameColumn(
                name: "FLM_DUREE",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_DUREE");

            migrationBuilder.RenameColumn(
                name: "FLM_DATEPARUTION",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_DATEPARUTION");

            migrationBuilder.RenameColumn(
                name: "FLM_ID",
                schema: "public",
                table: "T_E_FILM_FLM",
                newName: "FILM_ID");
        }
    }
}
