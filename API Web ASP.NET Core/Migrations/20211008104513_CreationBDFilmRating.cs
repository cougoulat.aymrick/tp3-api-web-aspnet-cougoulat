﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace API_Web_ASP.NET_Core.Migrations
{
    public partial class CreationBDFilmRating : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "Comptes",
                schema: "public",
                columns: table => new
                {
                    CPT_ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CPT_NOM = table.Column<string>(type: "varchar(50)", nullable: false),
                    CPT_PRENOM = table.Column<string>(type: "varchar(50)", nullable: false),
                    CPT_TELPORTABLE = table.Column<string>(type: "char(10)", nullable: true),
                    CPT_MEL = table.Column<string>(type: "varchar(100)", nullable: false),
                    CPT_PWD = table.Column<string>(type: "varchar(64)", nullable: true),
                    CPT_RUE = table.Column<string>(type: "varchar(200)", nullable: false),
                    CPT_CP = table.Column<string>(type: "char(5)", nullable: false),
                    CPT_VILLE = table.Column<string>(type: "varchar(50)", nullable: false),
                    CPT_PAYS = table.Column<string>(type: "varchar(50)", nullable: false, defaultValue: "France"),
                    CPT_LATITUDE = table.Column<float>(type: "real", nullable: true),
                    CPT_LONGITUDE = table.Column<float>(type: "real", nullable: true),
                    CPT_DATECREATION = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "current_date")
                },
                constraints: table =>
                {
                    table.PrimaryKey("compte_pk", x => x.CPT_ID);
                });

            migrationBuilder.CreateTable(
                name: "film",
                schema: "public",
                columns: table => new
                {
                    FILM_ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FILM_TITRE = table.Column<string>(type: "varchar(100)", nullable: false),
                    FILM_SYNOPSIS = table.Column<string>(type: "varchar(500)", nullable: true),
                    FILM_DATEPARUTION = table.Column<DateTime>(type: "date", nullable: false),
                    FILM_DUREE = table.Column<decimal>(type: "numeric(3,0)", nullable: false),
                    FILM_GENRE = table.Column<string>(type: "varchar(30)", nullable: false),
                    FILM_URLPHOTO = table.Column<string>(type: "varchar(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("film_pk", x => x.FILM_ID);
                });

            migrationBuilder.CreateTable(
                name: "Favoris",
                schema: "public",
                columns: table => new
                {
                    CPT_ID = table.Column<int>(type: "integer", nullable: false),
                    FLM_ID = table.Column<int>(type: "integer", nullable: false),
                    FAV_COMMENTAIRE = table.Column<string>(type: "varchar(500)", nullable: true),
                    Compte = table.Column<int>(type: "integer", nullable: true),
                    Film = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("favoris_pk", x => new { x.CPT_ID, x.FLM_ID });
                    table.ForeignKey(
                        name: "fk_favoris_compte",
                        column: x => x.CPT_ID,
                        principalSchema: "public",
                        principalTable: "Comptes",
                        principalColumn: "CPT_ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_favoris_film",
                        column: x => x.FLM_ID,
                        principalSchema: "public",
                        principalTable: "film",
                        principalColumn: "FILM_ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comptes_CPT_MEL",
                schema: "public",
                table: "Comptes",
                column: "CPT_MEL",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Favoris_FLM_ID",
                schema: "public",
                table: "Favoris",
                column: "FLM_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Favoris",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Comptes",
                schema: "public");

            migrationBuilder.DropTable(
                name: "film",
                schema: "public");
        }
    }
}
