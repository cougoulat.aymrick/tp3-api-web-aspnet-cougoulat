﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API_Web_ASP.NET_Core.Migrations
{
    public partial class EditTableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "film",
                schema: "public",
                newName: "T_E_FILM_FLM",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Favoris",
                schema: "public",
                newName: "T_J_FAVORI_FAV",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Comptes",
                schema: "public",
                newName: "T_E_COMPTE_CPT",
                newSchema: "public");

            migrationBuilder.RenameIndex(
                name: "IX_Favoris_FLM_ID",
                schema: "public",
                table: "T_J_FAVORI_FAV",
                newName: "IX_T_J_FAVORI_FAV_FLM_ID");

            migrationBuilder.RenameIndex(
                name: "IX_Comptes_CPT_MEL",
                schema: "public",
                table: "T_E_COMPTE_CPT",
                newName: "IX_T_E_COMPTE_CPT_CPT_MEL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "T_J_FAVORI_FAV",
                schema: "public",
                newName: "Favoris",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "T_E_FILM_FLM",
                schema: "public",
                newName: "film",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "T_E_COMPTE_CPT",
                schema: "public",
                newName: "Comptes",
                newSchema: "public");

            migrationBuilder.RenameIndex(
                name: "IX_T_J_FAVORI_FAV_FLM_ID",
                schema: "public",
                table: "Favoris",
                newName: "IX_Favoris_FLM_ID");

            migrationBuilder.RenameIndex(
                name: "IX_T_E_COMPTE_CPT_CPT_MEL",
                schema: "public",
                table: "Comptes",
                newName: "IX_Comptes_CPT_MEL");
        }
    }
}
