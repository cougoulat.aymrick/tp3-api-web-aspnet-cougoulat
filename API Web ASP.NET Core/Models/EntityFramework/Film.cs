﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API_Web_ASP.NET_Core.Models.EntityFramework;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace API_Web_ASP.NET_Core.Models.EntityFramework
{
    [Table("T_E_FILM_FLM")]
    public partial class Film
    {
        public Film()
        {
            Favoris = new HashSet<Favori>();
        }

        [Key]
        [Column("FLM_ID")]
        public int FilmId { get; set; }
        
        [Required]
        [Column("FLM_TITRE", TypeName = "varchar(100)")]
        public string Titre { get; set; }

        [Column("FLM_SYNOPSIS", TypeName = "varchar(500)")]
        public string Synopsis { get; set; }

        [Required]
        [Column("FLM_DATEPARUTION", TypeName = "date")]
        public DateTime DateParution { get; set; }

        [Required]
        [Column("FLM_DUREE", TypeName = "numeric(3, 0)")]
        public decimal Duree { get; set; }

        [Required]
        [Column("FLM_GENRE", TypeName = "varchar(30)")]
        public string Genre { get; set; }

        [Column("FLM_URLPHOTO", TypeName = "varchar(200)")]
        public string UrlPhoto { get; set; }

        [InverseProperty(nameof(Favori.FilmFavori))]
        public virtual ICollection<Favori> Favoris { get; set; }
    }
}
