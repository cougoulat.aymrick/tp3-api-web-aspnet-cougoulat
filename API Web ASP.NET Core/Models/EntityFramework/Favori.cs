﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;

namespace API_Web_ASP.NET_Core.Models.EntityFramework
{
    [Table("T_J_FAVORI_FAV")]
    public class Favori
    {
        public Favori() { }

        [Key]
        [Column("CPT_ID")]
        public int CompteId { get; set; }

        [Key]
        [Column("FLM_ID")]
        public int FilmId { get; set; }

        [Column("FAV_COMMENTAIRE", TypeName = "varchar(500)")]
        public string Commentaire { get; set; }

        [ForeignKey(nameof(Compte))]
        [InverseProperty("Favoris")]
        [JsonIgnore]
        public virtual Compte CompteFavori { get; set; }

        [ForeignKey(nameof(Film))]
        [InverseProperty("Favoris")]
        [JsonIgnore]
        public virtual Film FilmFavori { get; set; }
    }
}
