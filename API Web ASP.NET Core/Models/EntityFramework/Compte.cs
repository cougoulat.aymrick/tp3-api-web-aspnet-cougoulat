﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_Web_ASP.NET_Core.Models.EntityFramework
{
    [Table("T_E_COMPTE_CPT")]
    public class Compte
    {
        public Compte()
        {
            Favoris = new HashSet<Favori>();
        }

        [Key]
        [Column("CPT_ID")]
        public int CompteId { get; set; }

        [Required]
        [Column("CPT_NOM", TypeName = "varchar(50)")]
        public string Nom { get; set; }

        [Required]
        [Column("CPT_PRENOM", TypeName = "varchar(50)")]
        public string Prenom { get; set; }

        [Phone]
        [Display(Name = "Phone")]
        [RegularExpression(@"\d{10}", ErrorMessage = "Le téléphone portable doit contenir 10 chiffres")]
        [Column("CPT_TELPORTABLE", TypeName = "char(10)")]
        public string TelPortable { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La longeur d'un email doit être comprise entre 6 et 100 caractères.")]
        [Column("CPT_MEL", TypeName = "varchar(100)")]
        public string Mel { get; set; }

        [Display(Name = "Password")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W)[A-Za-z\d@$!%*?&]{6,10}$", ErrorMessage = "Le mot de passe doit contenir entre 6 et 10 caractères avec au moins 1 lettre majuscule, 1 chiffre et 1 caractère spécial")]
        [Column("CPT_PWD", TypeName = "varchar(64)")]
        public string Pwd { get; set; }

        [Required]
        [Column("CPT_RUE", TypeName = "varchar(200)")]
        public string Rue { get; set; }

        [Required]
        [Display(Name = "PostalCode")]
        [RegularExpression(@"\d{5}", ErrorMessage = "Le code postale doit contenir 5 chiffres")]
        [Column("CPT_CP", TypeName = "char(5)")]
        public string CodePostal { get; set; }

        [Required]
        [Column("CPT_VILLE", TypeName = "varchar(50)")]
        public string Ville { get; set; }

        [Required]
        [Column("CPT_PAYS", TypeName = "varchar(50)")]
        public string Pays { get; set; }

        [Column("CPT_LATITUDE", TypeName = "real")]
        public float? Latitude { get; set; }

        [Column("CPT_LONGITUDE", TypeName = "real")]
        public float? Longitude { get; set; }

        [Required]
        [Column("CPT_DATECREATION", TypeName = "date")]
        public DateTime DateCreation { get; set; }

        [InverseProperty(nameof(Favori.CompteFavori))]
        public virtual ICollection<Favori> Favoris { get; set; }
    }
}
