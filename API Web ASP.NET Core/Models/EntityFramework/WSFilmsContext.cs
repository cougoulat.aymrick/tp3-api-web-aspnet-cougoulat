﻿using System;
using API_Web_ASP.NET_Core.Models.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Logging;

#nullable disable

namespace API_Web_ASP.NET_Core.Models.EntityFramework
{
    public partial class WSFilmsContext : DbContext
    {
        public WSFilmsContext()
        {
        }

        public WSFilmsContext(DbContextOptions<WSFilmsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Favori> Favoris { get; set; }
        public virtual DbSet<Compte> Comptes { get; set; }
        public virtual DbSet<Film> Films { get; set; }

        public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                // #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseLoggerFactory(MyLoggerFactory)
                    .EnableSensitiveDataLogging()
                    .UseNpgsql("Server=localhost;port=5432;Database=WSFilms;uid=postgres;password=root;");
                // optionsBuilder.UseLazyLoadingProxies();
            }
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.HasAnnotation("Relational:Collation", "French_France.1252");

            modelBuilder.Entity<Favori>(entity =>
            {
                entity.HasKey(e => new { e.CompteId, e.FilmId })
                    .HasName("favoris_pk");

                entity.HasOne(d => d.CompteFavori)
                    .WithMany(p => p.Favoris)
                    .HasForeignKey(d => d.CompteId)
                    .HasConstraintName("fk_favoris_compte");

                entity.HasOne(d => d.FilmFavori)
                    .WithMany(p => p.Favoris)
                    .HasForeignKey(d => d.FilmId)
                    .HasConstraintName("fk_favoris_film");
            });

            modelBuilder.Entity<Compte>(entity =>
            {
                entity.HasKey(e => e.CompteId).HasName("compte_pk");
                entity.HasIndex(c => c.Mel).IsUnique();
                entity.Property(c => c.Pays).HasDefaultValue("France");
                entity.Property(c => c.DateCreation).HasDefaultValueSql("current_date");
            });

            modelBuilder.Entity<Film>(entity =>
            {
                entity.HasKey(e => e.FilmId).HasName("film_pk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
