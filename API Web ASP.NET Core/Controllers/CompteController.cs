﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API_Web_ASP.NET_Core.Models.EntityFramework;
using Microsoft.AspNetCore.JsonPatch;
using API_Web_ASP.NET_Core.Models.DataManager;
using API_Web_ASP.NET_Core.Models.Repository;

namespace API_Web_ASP.NET_Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompteController : ControllerBase
    {
        private readonly IDataRepository<Compte> _dataRepository;

        public CompteController(IDataRepository<Compte> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        /// <summary>
        /// Get all users account
        /// </summary>
        /// <returns>Http response</returns>
        // GET: api/Compte
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Compte>>> GetComptes()
        {

            return _dataRepository.GetAll();
        }

        /// <summary>
        /// Get an account from his id
        /// </summary>
        /// <returns>Http response</returns>
        // GET: api/Compte/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Compte>> GetCompteById(int id)
        {
            var compte = _dataRepository.GetById(id);

            if (compte == null)
            {
                return NotFound("Id Compte inconnu");
            }

            return compte;
        }

        // PUT: api/Compte/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompte(int id, Compte compte)
        {
            if (id != compte.CompteId)
            {
                return BadRequest("Id compte inconnu");
            }

            var compteToUpdate = _dataRepository.GetById(id);

            if (compteToUpdate == null)
            {
                return NotFound();
            }

            _dataRepository.Update(compteToUpdate.Value, compte);

            return NoContent();
        }

        // POST: api/Compte
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Compte>> PostCompte(Compte compte)
        {
            _dataRepository.Add(compte);

            return CreatedAtAction(
                "GetCompte", 
                new { id = compte.CompteId }, 
                compte);
        }

        // DELETE: api/Compte/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Compte>> DeleteCompte(int id)
        {
            var compte = _dataRepository.GetById(id);
            if (compte == null)
            {
                return NotFound();
            }
            _dataRepository.Delete(compte.Value);
            return compte;
        }

        [HttpGet("email/{email}")]
        public async Task<ActionResult<Compte>> GetCompteByEmail(string email)
        {
            var compte = _dataRepository.GetByString(email);

            if (compte == null)
            {
                return NotFound("Email Compte inconnu");
            }

            return compte;
        }

        /*[HttpPatch("{id:int}")]
        public async Task<ActionResult<Compte>> Patch(int id, [FromBody] JsonPatchDocument<Compte> patchEntity)
        {
            var entity = _context.Comptes.FirstOrDefault(c => c.CompteId == id);

            if (entity == null)
            {
                return NotFound();
            }

            patchEntity.ApplyTo(entity, ModelState); // Must have Microsoft.AspNetCore.Mvc.NewtonsoftJson installed

            return entity;
        }*/
    }
}
