﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using WindowsAppTP3.Models;
using Newtonsoft.Json;

namespace WindowsAppTP3.Services
{
    class WSService
    {
        private static WSService instance = null;

        public static WSService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WSService();
                }
                return instance;
            }
        }

        HttpClient http = new HttpClient();

        private WSService()
        {
            http.BaseAddress = new Uri("https://localhost:44387/api/");
            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<Compte> getCompteByEmail(string mel)
        {
            Compte compte = new Compte();

            HttpResponseMessage res = await http.GetAsync($"Compte/email/" + mel);
            if (res.IsSuccessStatusCode)
            {
                var jsonString = await res.Content.ReadAsStringAsync();
                compte = JsonConvert.DeserializeObject<Compte>(jsonString);
            }

            return compte;
        }

        public async void addCompte(Compte compte)
        {
            var jsonCompte = JsonConvert.SerializeObject(compte);
            StringContent queryString = new StringContent(jsonCompte);
            HttpResponseMessage res = await http.PostAsync($"Compte/", queryString);
        }
    }
}
