﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WindowsAppTP3.Models
{
    public class Favori
    {
        public Favori() { }

        public int CompteId { get; set; }

        public int FilmId { get; set; }

        public string Commentaire { get; set; }

        public virtual Compte CompteFavori { get; set; }

        public virtual Film FilmFavori { get; set; }
    }
}
