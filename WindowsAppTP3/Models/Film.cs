﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WindowsAppTP3.Models
{
    public partial class Film
    {
        public Film()
        {
            Favoris = new HashSet<Favori>();
        }

        public int FilmId { get; set; }
        
        public string Titre { get; set; }

        public string Synopsis { get; set; }

        public DateTime DateParution { get; set; }

        public decimal Duree { get; set; }

        public string Genre { get; set; }

        public string UrlPhoto { get; set; }

        public virtual ICollection<Favori> Favoris { get; set; }
    }
}
