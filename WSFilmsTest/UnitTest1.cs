using API_Web_ASP.NET_Core.Controllers;
using API_Web_ASP.NET_Core.Models.DataManager;
using API_Web_ASP.NET_Core.Models.EntityFramework;
using API_Web_ASP.NET_Core.Models.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace WSFilmsTest
{
    [TestClass]
    public class UnitTest1
    {
        private WSFilmsContext _context;
        private CompteController _controller;
        private IDataRepository<Compte> _dataRepository;

        public UnitTest1()
        {
            var builder = new DbContextOptionsBuilder<WSFilmsContext>().UseNpgsql("Server=localhost;port=5432;Database=WSFilms;uid=postgres;password=root;");
            _context = new WSFilmsContext(builder.Options);
            _dataRepository = new CompteManager(_context);
            _controller = new CompteController(_dataRepository);
        }
        [TestMethod]
        public void PostCompte_ModelValidated_CreationOK()
        {
            // Arrange
            Random rnd = new Random();
            int chiffre = rnd.Next(1, 1000000000);
             Compte compteAtester = new Compte()
             {
                 Nom = "MACHIN",
                 Prenom = "Luc",
                 TelPortable = "0606070809",
                 Mel = "machin" + chiffre + "@gmail.com",
                 Pwd = "Toto1234!",
                 Rue = "Chemin de Bellevue",
                 CodePostal = "74940",
                 Ville = "Annecy-le-Vieux",
                 Pays = "France",
                 Latitude = null,
                 Longitude = null
             };
            // Act
            var result = _controller.PostCompte(compteAtester).Result;
            var result2 = _controller.GetCompteByEmail(compteAtester.Mel);
            var actionResult = result2.Result as ActionResult<Compte>;
            // Assert
            Assert.IsInstanceOfType(actionResult.Value, typeof(Compte), "Pas un compte");
            Compte compteRecupere = _context.Comptes.Where(c => c.Mel ==
           compteAtester.Mel).FirstOrDefault();
            compteAtester.CompteId = compteRecupere.CompteId;
            Assert.AreEqual(compteRecupere, compteAtester, "Comptes pas identiques");
        }
    }
}
